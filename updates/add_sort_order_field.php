<?php namespace JMSystems\Gallery\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddSortOrderField extends Migration
{
    public function up()
    {
        Schema::table('jmsystems_gallery_galleries', function (Blueprint $table) {
            $table->integer('sort_order')->default(0);
        });
    }

    public function down()
    {
        Schema::table('jmsystems_gallery_galleries', function (Blueprint $table) {
            $table->dropColumn('sort_order');
        });
    }
}
