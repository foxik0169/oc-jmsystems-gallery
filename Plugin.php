<?php namespace JMSystems\Gallery;

use Event;
use Backend;
use JMSystems\Gallery\Models\Gallery;
use System\Classes\PluginBase;

/**
 * Gallery Plugin Information File
 */
class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'jmsystems.gallery::lang.plugin.name',
            'description' => 'jmsystems.gallery::lang.plugin.description',
            'author'      => 'JM Systems',
            'icon'        => 'icon-image'
        ];
    }

    public function boot()
    {
        Event::listen('translate.localePicker.translateParams', function($page, $params, $oldLocale, $newLocale) {

            // Translate slug parameter if page contains detail component and it's value is driven from route param.

            if (!$page->hasComponent('jmsGalleryDetail')) {
                return;
            }

            $component = $page->getComponent('jmsGalleryDetail');
            $componentSlugProperty = $component->property('slug');

            // Parse property with regex because the component is not yet initialized and the method paramName() is not available here.

            if (!preg_match('/\{\{.+\:(.+).+\}\}/', $componentSlugProperty, $matches)) {
                return;
            }

            $slugParam = $matches[1];

            if (!array_key_exists($slugParam, $params)) {
                return;
            }

            $gallery = Gallery::transWhere('slug', $params[$slugParam])->first();

            return [
                $slugParam => $gallery->getAttributeTranslated('slug', $newLocale)
            ];

        });
    }

    public function registerComponents()
    {
        return [
            'JMSystems\Gallery\Components\GalleryDetail' => 'jmsGalleryDetail',
            'JMSystems\Gallery\Components\GalleryList'   => 'jmsGalleryList'
        ];
    }

    public function registerPermissions()
    {
        return [
            'jmsystems.gallery.manage' => [
                'label'         => 'jmsystems.gallery::lang.permissions.manage.title',
                'description'   => 'jmsystems.gallery::lang.permissions.manage.description',
                'tab'         => 'jmsystems.gallery::lang.permissions.manage.tab'
            ]
        ];
    }

    public function registerNavigation()
    {
        return [
            'galleries' => [
                'label'       => 'jmsystems.gallery::lang.navigation.galleries',
                'url'         => Backend::url('jmsystems/gallery/galleries'),
                'icon'        => 'icon-image',
                'permissions' => ['jmsystems.gallery.manage'],
                'order'       => 500,
            ],

        ];
    }

}
