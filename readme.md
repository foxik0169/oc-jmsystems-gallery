![Logo](assets/image/icon-large.png)

# JM Systems / Gallery

Minimal multilingual gallery plugin for October CMS that doens't provide specific frontend. This plugin attempts to provide a bare implementation of gallery backend that can be extended for project-specific functionality (e.g. add fields to the database entry, provide snippets for static pages, etc.).

## Features

 - create, edit, sort galleries and images inside gallery
 - fully translatable with automatic route parameter translation
 - image preview and count columns in the listing (can be hidden)

## Models

### Gallery

A simple gallery model with `title`, `slug` fields, and `images` relationship.

## Components

### Gallery list

Provides access to a list of all galleries. The component exposes two getters: `galleryList.items` which returns all gallery entries without the attachment files and `galleryList.itemsWithImages` which returns galleries with the images.

### Gallery detail

Provides access to a single gallery entry. Can be accessed by getter `galleryDetail.model`.

Gallery detail component has two modes of operation. You can specify the ID of the gallery to display by a dropdown menu, or provide a slug parameter that can be mapped into a route parameter. This route parameter will then be automatically translated when requested by Translate plugin.

## License

This plugin is licensed under MIT license.

Created by [Daniel Jenikovský](https://danieljenikovsky.sk).
