<?php

return [
    'plugin' => [
        'name'        => 'JM Systems / Galéria',
        'description' => 'Minimálny viac jazyčný modul galérie, ktorý neposkytuje špecifický frontend.',

    ],

    'permissions' => [
        'manage' => [
            'title'       => 'Správa galérií',
            'description' => 'Povoliť použivateľovi vytvárať, editovať a zmazať galérie na tejto stránke.',
            'tab'         => 'JM Systems - Galéria'
        ]
    ],

    'navigation' => [
        'galleries' => 'Galérie'
    ],

    'components' => [
        'list' => [
            'name'        => 'List galérií',
            'description' => 'Poskytuje prístup k listu existujúcich galérií.'
        ],
        'detail' => [
            'name'        => 'Detail galérie',
            'description' => 'Poskytuje prístup ku konkrétnej galérií na základe adresy alebo výberu.',

            'properties'  => [
                'slug'      => 'Adresa',
                'gallery'   => 'Galéria'
            ]

        ]
    ],

    'fields' => [
        'title'     => 'Názov',
        'slug'      => 'Adresa',
        'images'    => 'Obrázky',

        'sort'    => 'Poradie',
        'preview'      => 'Náhľad',
        'imagesCount' => 'Počet obrázkov'

    ],

    'controllers' => [
        'list' => [
            'title' => 'Spravovať galérie',
            'noRecordsMessage' => 'Žiadne galérie neboli nájdené.',
        ],

        'form' => [
            'name'  => 'Galéria',
            'create' => 'Vytvoriť galériu',
            'update' => 'Upraviť galériu',
            'preview' => 'Náhľad galérie'
        ],

        'reorder' => [
            'title' => 'Zmeniť poradie galérií',
        ]

    ],

    'indication' => [
        'creating' => 'Vytváram galériu...',
        'saving'   => 'Ukladám galériu...'
    ],

    'actions' => [
        'create'         => 'Vytvoriť',
        'createAndClose' => 'Vytvoriť a zavrieť',
        'save'           => 'Uložiť',
        'saveAndClose'   => 'Uložiť a zavrieť',
        'or'             => 'alebo',
        'cancel'         => 'Zrušiť',
        'returnToList'   => 'Späť na zoznam galérií',

        'new'            => 'Nová galéria',
        'reorder'        => 'Zmeniť poradie',
        'delete'         => 'Zmazať vybrané',
        'deletePrompt'   => 'Ste si istý že chcete vymazať vybrané galérie?',

        'back' => 'Späť'

    ]

];
