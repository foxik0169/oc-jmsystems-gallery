<?php

return [
    'plugin' => [
        'name'        => 'JM Systems / Gallery',
        'description' => 'Minimal multilingual gallery plugin that doens\'t provide specific frontend.',
    ],

    'permissions' => [
        'manage' => [
            'title'       => 'Manage galleries',
            'description' => 'Allow user to create, edit and remove galleries at on this site.',
            'tab'         => 'JM Systems - Gallery'
        ]
    ],

    'navigation' => [
        'galleries' => 'Galleries'
    ],

    'components' => [
        'list' => [
            'name'        => 'Gallery list component',
            'description' => 'Provides access to list of existing galleries.'
        ],
        'detail' => [
            'name'        => 'Gallery detail component',
            'description' => 'Provides access to a specified gallery by slug or selection.',

            'properties'  => [
                'slug'      => 'Slug',
                'gallery'   => 'Gallery'
            ]

        ]
    ],

    'fields' => [
        'title'     => 'Title',
        'slug'      => 'Slug',
        'images'    => 'Images',

        'sort'    => 'Sort',
        'preview'      => 'Preview',
        'imagesCount' => 'Image count'

    ],

    'controllers' => [
        'list' => [
            'title' => 'Manage Galleries',
            'noRecordsMessage' => 'No galleries found.',
        ],

        'form' => [
            'name'  => 'Gallery',
            'create' => 'Create Gallery',
            'update' => 'Edit Gallery',
            'preview' => 'Preview Gallery'
        ],

        'reorder' => [
            'title' => 'Reorder Galleries',
        ]

    ],

    'indication' => [
        'creating' => 'Creating Gallery...',
        'saving'   => 'Saving Gallery...'
    ],

    'actions' => [
        'create'         => 'Create',
        'createAndClose' => 'Create and Close',
        'save'           => '<u>S</u>ave',
        'saveAndClose'   => 'Save and Close',
        'or'             => 'or',
        'cancel'         => 'Cancel',
        'returnToList'   => 'Return to galleries list',

        'new'            => 'New Gallery',
        'reorder'        => 'Change order',
        'delete'         => 'Delete selected',
        'deletePrompt'   => 'Are you sure you want to delete the selected Galleries?',

        'back' => 'Back'

    ]

];
