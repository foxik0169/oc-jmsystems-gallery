<?php namespace JMSystems\Gallery\Models;

use Model;

/**
 * Gallery Model
 */
class Gallery extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    public $table = 'jmsystems_gallery_galleries';

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel'
    ];

    public $translatable = [
        'title',
        ['slug', 'index' => true]
    ];

    protected $guarded = ['*'];

    protected $fillable = [
        'title',
        'slug'
    ];

    public $rules = [
        'title' => 'required|max:200',
        'slug'  => 'required|max:200|unique:jmsystems_gallery_galleries,slug'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'images' => 'System\Models\File'
    ];

}
