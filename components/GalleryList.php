<?php namespace JMSystems\Gallery\Components;

use ArrayAccess;
use Cms\Classes\ComponentBase;
use JMSystems\Gallery\Models\Gallery;

class GalleryList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'jmsystems.gallery::lang.components.list.name',
            'description' => 'jmsystems.gallery::lang.components.list.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function items()
    {
        return Gallery::all();
    }

    public function itemsWithImages()
    {
        return Gallery::with('images')->get();
    }

}
