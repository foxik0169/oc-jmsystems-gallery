<?php namespace JMSystems\Gallery\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use JMSystems\Gallery\Models\Gallery;

class GalleryDetail extends ComponentBase
{

    protected $gallery;

    public function componentDetails()
    {
        return [
            'name'        => 'jmsystems.gallery::lang.components.detail.name',
            'description' => 'jmsystems.gallery::lang.components.detail.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'jmsystems.gallery::lang.components.detail.properties.slug',
                'type' => 'string',
            ],
            'galleryId' => [
                'title' => 'jmsystems.gallery::lang.components.detail.properties.gallery',
                'type' => 'dropdown',
                'showExternalParam' => false
            ]
        ];
    }

    public function getGalleryIdOptions()
    {
        return Gallery::select(['id', 'title'])->get()->pluck('title', 'id')->prepend('(none)', -1)->toArray();
    }

    public function onRun()
    {
        $galleryId = $this->property('galleryId');
        $slug = $this->property('slug');

        if ($slug) {
            if (class_exists('RainLab\Translate\Behaviors\TranslatableModel')) {
                $this->gallery = Gallery::transWhere('slug', $slug)->first();
            } else {
                $this->gallery = Gallery::where('slug', $slug)->first();
            }

        } else if ($galleryId !== '-1') {
            $this->gallery = Gallery::where('id', $id)->first();
        }

    }

    public function model()
    {
        return $this->gallery;
    }

}
